use iced::{
    executor,
    widget::{button, column, container, row, text},
    Application, Command, Length, Settings,
};

fn main() {
    let res = App::run(Settings::default());

    match res {
        Ok(_) => {}
        Err(e) => {
            eprintln!("Error {}", e);
        }
    }
}

#[derive(Debug, Clone)]
enum Message {
    ToggleLightmode,
}

enum State {
    Loading,
    Normal,
}

struct App {
    state: State,
    lightmode: bool,
}

impl Application for App {
    type Message = Message;
    type Executor = executor::Default;
    type Theme = iced::Theme;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, iced::Command<Self::Message>) {
        let self_i = Self {
            state: State::Normal,
            lightmode: false
        };

        return (self_i, Command::none());
    }

    fn title(&self) -> String {
        String::from("Thorn")
    }

    fn update(&mut self, message: Self::Message) -> iced::Command<Self::Message> {
        match message {
            Message::ToggleLightmode => {
                self.lightmode = !self.lightmode;
            },
            _ => {
                return Command::none();
            }
        }

        return Command::none();
    }

    fn view(&self) -> iced::Element<'_, Self::Message, iced::Renderer<Self::Theme>> {
        let logo_text = text("Thorn");

        let sidebar = column!(logo_text).height(Length::Fill);
        
        let main_row = row!(sidebar).width(Length::Fill);

        container(main_row).height(Length::Fill).into()
    }

    fn theme(&self) -> Self::Theme {
        if self.lightmode {
            iced::Theme::Light
        } else {
            iced::Theme::Dark
        }
    }
}
