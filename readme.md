# Thorn

Program for taking notes, todos and making pdfs using a markdown-like syntax in a visual editor. Made in rust using iced.rs

# Mockup

![Mockup of Thorn, made in Figma](./mockup.png)

# Todo
 - [ ] Add a window where you can write text and notes
 - [ ] Save notes to files
 - [ ] Create custom iced widget for rendering text
     - [ ] Vim/helix motions for editing and moving
     - [ ] Add support for headers/sections
     - [ ] Add support for lists (ordered and unordered)
     - [ ] Add support for table of contents
     - [ ] Add support for images
     - [ ] Add support for linking to other files/notes
     - [ ] Add support for citations
     - [ ] Add procedural blocks where you can write expressions to create text (eg. a for loop to create multiple paragraphs)
 - [ ] Create overlay trait for adding menus on top of menus
     - [ ] Create fuzzy file finder menu
     - [ ] Create fuzzy search menu

# Experience (TODO: find a better name for this section)

When using the editor lines will automatically be parsed and rendered as they will show up when exported (if you decide to export)

When the text cursor is on a line it will display its raw form, IE. Section headers will display `# Section`, codeblocks will display their
code instead of their evaluated output.

# Controls

The editor is planned to be used with vim/helix like motions first, and regular text editing second. The option to change between them will be added to the settings.

# Syntax

Syntax is planned to be like markdown, using certain characters and context for special features.
Eg. sections will be started with `# Section`, subsections with `## Subsection`, etc.
Hashtags and other special characters will be able to be escaped using a backslash (\\) which will be done automatically by the editor unless you configure it otherwise. Motions and keybinds will be used to
create special blocks by default

## Codeblocks

Planned features include procedual blocks, where you can write code to generate text. This can be used for generating multiple instances of text with only 1 thing changed.
Code blocks would be made by enclosing code in a hastag and square bracket `#[ code #]`. Eg.
```rs
#[
 for i in 1..5:
   text("This is paragraph " + (i + 1)); 
#]
```
would create 5 lines with the text "This is paragraph 1-5".
In the editor the evaluated block would be shown with a faint green outline to highlight that it is generated using a code block. The final exported pdf will not include the outline.
Double clicking the evaluated block or using the assigne motion would open its code and allow for changes to be made.

## Images

Images are a must have for taking notes. Borrowing from markdown, images will be embedded using `![description](url){width:n, height:m, ...}` and displayed as images in the editor.
The images include a curly block for settings for specifying the dimensions of the image or how text wraps around it. It can be ommited, or set to {default} to automatically fill the entire width of the page.

The editor will automatically take care of embedding images that are added using the menu or drag and drop.

## Math

Using the program to solve math expressions is paramount for my usecase, so it will include a special math block. 
It comes in 2 variants, Evaluable and NonEvaluable. Evaluable math blocks will automatially evaluate their contents upon exiting them, NonEvaluable will simply display their contents
in a pretty way (subscript, superscript, fractions etc.).

Math blocks are created with `#calc[ ... #]` and NonEvaluable variants are created with `#math[ ... #]`. They can be multiline just like procedural blocks

For now, Qalculate! will be used for solving expressions, but i plan on creating my own solver in the future.

### Graphing

For graphing functions in math you will have to write the function as a ![Codeblock][codeblock] and display it from there for now.

In the future the custom solver objects will be available through an api for codeblocks and something like 
```rs
#[
 let image = new Image(800, 600);
 Math::GetFunctionByName("f(x)").graph(image, ...);
#]
```

# Installing

Todo

# Building

`cargo build`
